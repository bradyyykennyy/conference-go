from .keys import WeatherData, GeoData, PexelsData, CountryCode
import requests
import json



def get_photo(city, state):

    paramaters = {'query': f"{city} {state}", 'size': 'small', 'page': 1, 'per_page': 3}

    response = requests.get(PexelsData["URL"], headers = PexelsData["Headers"], params = paramaters)
    response = json.loads(response.content)
    photo_url = response["photos"][2]["url"]

    return photo_url


def get_weather_data(city, state):

    GEO_Paramaters = {'q': f"{city},{state},{CountryCode}", 'appid': GeoData["API_KEY"], 'limit': 4}
    GEO_Response = requests.get(GeoData["URL"], headers = GeoData["Headers"], params = GEO_Paramaters)
    GEO_Response = json.loads(GEO_Response.content)[0]

    Weather_Paramaters = {'lat': GEO_Response["lat"], 'lon': GEO_Response["lon"],'appid': WeatherData["API_KEY"], 'units':"imperial"}
    Weather_Response = requests.get(WeatherData["URL"], headers = WeatherData["Headers"], params = Weather_Paramaters)
    Weather_Response = json.loads(Weather_Response.content)

    return {"temp": Weather_Response["main"]["temp"], "description": Weather_Response["weather"][0]["description"]}
