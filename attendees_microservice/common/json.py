from json import JSONEncoder
from datetime import datetime
from django.db.models import QuerySet

class DateEncoder():
    def default(self, object):
        if isinstance(object, datetime): return object.isoformat()
        else: return super().default(object)


class QuerySetEncoder(JSONEncoder):
    def default(self, object):
        if isinstance(object, QuerySet): return list(object)
        else: return super().default(object)


class ModelEncoder(DateEncoder, QuerySetEncoder, JSONEncoder):

    encoders = {}

    def default(self, object):

        if isinstance(object, self.model):

            #ReturnDict = {property: getattr(object, property) for property in self.properties}
            ReturnDict = {}

            for property in self.properties:

                value = getattr(object, property)

                if property in self.encoders:
                    encoder = self.encoders[property]
                    value = encoder.default(value)

                ReturnDict[property] = value


            if getattr(object, 'get_api_url'): ReturnDict["href"] = object.get_api_url()

            ReturnDict.update(self.get_extra_data(object))

            return ReturnDict

        else: return super().default(object)

    def get_extra_data(self, object):
        return {}
